from django.urls import path
from django.views.decorators.cache import cache_page

from .api import ProfileAutocompleteSearch
from .views import (
    MyProfileDetail,
    MyProfileDetailExport,
    MyProfileUpdate,
    UserCreate,
    UserDelete,
    UserDetail,
    UserListView,
    UserPassword,
    UserUpdate,
    change_password,
)

# from django.contrib.auth.paths import views
app_name = "accounts"

urlpatterns = [
    path("create", UserCreate.as_view(), name="user_create"),
    path("<int:pk>/update", UserUpdate.as_view(), name="user_update"),
    path("<int:pk>/password", UserPassword.as_view(), name="user_password"),
    path("<int:pk>/delete", UserDelete.as_view(), name="user_delete"),
    path("<int:pk>/detail", UserDetail.as_view(), name="user_detail"),
    path("myprofile/change_password", change_password, name="change_password"),
    path("myprofile/detail", MyProfileDetail.as_view(), name="myprofile_detail"),
    path(
        "myprofile/detail/export",
        MyProfileDetailExport.as_view(),
        name="myprofile_detail_export",
    ),
    path(
        "api/v1/profile/search",
        cache_page(60 * 15)(ProfileAutocompleteSearch.as_view()),
        name="profile_search_api",
    ),
    path("myprofile/update", MyProfileUpdate.as_view(), name="myprofile_update"),
    path("list", UserListView.as_view(), name="user_search"),
]
