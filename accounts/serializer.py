from rest_framework import serializers

from .models import Profile


class ProfileSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ("id", "get_full_name")
