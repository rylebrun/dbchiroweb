# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.db.models.query import QuerySet
from rest_framework import generics

from sights.models import Session

from .models import Profile
from .serializer import ProfileSearchSerializer

# Get an instance of a logger
logger = logging.getLogger(__name__)


def filterUser(qs: QuerySet, q: str) -> QuerySet:

    return qs.filter(
        Q(username__icontains=q)
        | Q(first_name__icontains=q)
        | Q(last_name__icontains=q)
        | Q(email__icontains=q)
    )


class ProfileAutocompleteSearch(
    LoginRequiredMixin,
    generics.ListAPIView,
):
    pagination_class = None
    serializer_class = ProfileSearchSerializer

    def get_queryset(self):
        qs = Profile.objects

        q = self.request.query_params.get("q", None)
        id = self.request.query_params.get("id", None)
        session = self.request.query_params.get("session", None)
        logger.debug(f"ProfileAutocompleteSearch API querystring {q} {id} {session}")

        if id:
            qs = qs.filter(id=int(id))
            result = qs.all()
        if q and not id:
            if session:
                mainobs = filterUser(Session.objects.get(pk=session).main_observer.all(), q)
                otherobs = filterUser(Session.objects.get(pk=session).other_observer.all(), q)
                result = set(mainobs + otherobs)
            else:
                qs = filterUser(qs, q)
                result = qs.order_by("get_full_name").order_by("is_active").all()
        logger.debug(f"ProfileAutocompleteSearch API result {result}")
        return result
