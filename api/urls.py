# -*- coding: utf-8 -*-

# from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import (  # GeoJSONTiledPlaceData,
    ActiveUserAutocomplete,
    AllUserAutocomplete,
    MetaPlaceAutocomplete,
    MunicipalityAutocomplete,
    PlaceAutocomplete,
    StudyAutocomplete,
    TaxaAutocomplete,
    TerritoryAutocomplete,
    metaplaces_as_geojson,
    places_as_geojson,
    places_in_metaplace_as_geojson,
)

# from sights.models import Municipality, Territory


# from djgeojson.views import GeoJSONLayerView


app_name = "general_api"

urlpatterns = [
    # path(
    #     "municipality.geojson",
    #     login_required(
    #         GeoJSONLayerView.as_view(
    #             model=Municipality,
    #             precision=3,
    #             simplify=0.5,
    #             properties=("name", "code"),
    #         )
    #     ),
    #     name="geodata_municipality",
    # ),
    # path(
    #     "territory.geojson",
    #     login_required(
    #         GeoJSONLayerView.as_view(
    #             model=Territory, precision=3, simplify=0.5, properties=("name", "code")
    #         )
    #     ),
    #     name="geodata_territory",
    # ),
    path("place.geojson", places_as_geojson, name="geodata_place"),
    path("metaplaces.geojson", metaplaces_as_geojson, name="geodata_metaplaces"),
    path(
        "metaplaces/places.geojson",
        places_in_metaplace_as_geojson,
        name="geodata_places_in_metaplaces",
    ),
    path(
        "metaplaces/<int:pk>/places.geojson",
        places_in_metaplace_as_geojson,
        name="geodata_place_in_metaplace",
    ),
    # path(
    #     "place/<int:z>/<int:x>/<int:y>",
    #     GeoJSONTiledPlaceData.as_view(),
    #     name="placetiled",
    # ),
    path(
        "active-user-autocomplete/",
        ActiveUserAutocomplete.as_view(),
        name="active_user_autocomplete",
    ),
    path(
        "all-user-autocomplete/",
        AllUserAutocomplete.as_view(),
        name="all_user_autocomplete",
    ),
    path("taxa-autocomplete/", TaxaAutocomplete.as_view(), name="taxa_autocomplete"),
    path(
        "municipality-autocomplete/",
        MunicipalityAutocomplete.as_view(),
        name="municipality_autocomplete",
    ),
    path(
        "territory-autocomplete/",
        TerritoryAutocomplete.as_view(),
        name="territory_autocomplete",
    ),
    path("place-autocomplete/", PlaceAutocomplete.as_view(), name="place_autocomplete"),
    path(
        "metaplace-autocomplete/",
        MetaPlaceAutocomplete.as_view(),
        name="metaplace_autocomplete",
    ),
    path(
        "v1/study/search",
        StudyAutocomplete.as_view(),
        name="study_autocomplete",
    ),
]
