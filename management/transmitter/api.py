from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from rest_framework.generics import ListAPIView

from ..models import Transmitter
from .serializers import SearchTransmitterSerializer


class TransmitterSearchApi(
    LoginRequiredMixin,
    ListAPIView,
):
    serializer_class = SearchTransmitterSerializer
    pagination_class = None

    def get_queryset(self, *args, **kwargs):

        qs = Transmitter.objects

        available = self.request.query_params.get("available", None)
        q = self.request.query_params.get("q", None)
        id = self.request.query_params.get("id", None)
        qs = qs.filter(pk=id) if id else qs
        qs = qs.filter(available=True) if available else qs
        qs = qs.filter(Q(name__icontains=q) | Q(reference__icontains=q)) if q else qs

        return qs.order_by("-timestamp_update")
