def log_check(object):
    return (
        f"\nname\t{type(object).__name__}\nvalue\t{object}\n"
        f"type\t{type(object)}\ndir\t{dir(object)}"
    )
