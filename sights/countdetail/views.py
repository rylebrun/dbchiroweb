"""
    Vues de l'application Sights
"""

import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from core import js
from dicts.models import CountPrecision

from ..forms import (
    CountDetailAcousticForm,
    CountDetailBiomForm,
    CountDetailOtherForm,
    CountDetailTelemetryForm,
)
from ..mixins import CountDetailEditAuthMixin
from ..models import CountDetail, Sighting

logger = logging.getLogger(__name__)


#################################################
#             CountDetail model views           #
#################################################


class CountDetailCreate(LoginRequiredMixin, CreateView):
    model = CountDetail
    template_name = "normal_form.html"

    def get_form_class(self):
        sighting_id = self.kwargs.get("pk")
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        if contact in ("vm", "ca"):
            return CountDetailBiomForm
        if contact == "du":
            return CountDetailAcousticForm
        if contact == "te":
            return CountDetailTelemetryForm
        else:
            return CountDetailOtherForm

    def get_form_kwargs(self):
        kwargs = super(CountDetailCreate, self).get_form_kwargs()
        sighting_id = self.kwargs.get("pk")
        kwargs["sighting_id"] = sighting_id
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        kwargs["contact"] = contact
        if contact in ("vm", "ca"):
            kwargs["session"] = Sighting.objects.get(id_sighting=sighting_id).session.id_session
        return kwargs

    def get_initial(self):
        initial = super(CountDetailCreate, self).get_initial()
        initial = initial.copy()
        sighting_id = self.kwargs.get("pk")
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        if contact in ("vm", "ca"):
            initial["manipulator"] = self.request.user
            initial["validator"] = self.request.user
        if CountDetail.objects.filter(sighting_id=sighting_id):
            lastcountdetail = CountDetail.objects.filter(sighting_id=sighting_id).order_by(
                "-timestamp_create"
            )[0]
            if contact in ("vm", "du"):
                if lastcountdetail:
                    initial["time"] = lastcountdetail.time
                    initial["method"] = lastcountdetail.method
                    initial["unit"] = lastcountdetail.unit
                    initial["precision"] = lastcountdetail.precision
                    initial["device"] = lastcountdetail.device
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.sighting_id = self.kwargs.get("pk")
        contact = Sighting.objects.get(id_sighting=self.kwargs.get("pk")).session.contact.code
        if contact == "du":
            form.instance.precision = CountPrecision.objects.get(contact=contact)
        if contact in ("vm", "te"):
            form.instance.count = 1
        return super(CountDetailCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CountDetailCreate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-venus-mars"
        context["title"] = _("Ajout d'une observation détaillée")
        context["js"] = js.TimeInput
        return context

    def get_success_url(self):
        sighting_id = self.object.sighting_id
        if self.request.method == "POST" and "_addanother" in self.request.POST:
            logger.debug("lala")
            return reverse_lazy("sights:countdetail_create", kwargs={"pk": sighting_id})
        return reverse_lazy("sights:sighting_detail", kwargs={"pk": self.object.sighting_id})


class CountDetailUpdate(CountDetailEditAuthMixin, UpdateView):
    model = CountDetail
    template_name = "normal_form.html"

    def get_form_class(self):
        sighting_id = self.object.sighting_id
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        if contact in ("vm", "ca"):
            return CountDetailBiomForm
        if contact == "du":
            return CountDetailAcousticForm
        if contact == "te":
            return CountDetailTelemetryForm
        else:
            return CountDetailOtherForm

    def get_form_kwargs(self):
        kwargs = super(CountDetailUpdate, self).get_form_kwargs()
        sighting_id = self.object.sighting_id
        kwargs["sighting_id"] = sighting_id
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        kwargs["contact"] = contact
        if contact in ("vm", "ca"):
            kwargs["session"] = Sighting.objects.get(id_sighting=sighting_id).session.id_session
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        contact = Sighting.objects.get(id_sighting=self.object.sighting_id).session.contact.code
        if contact == "du":
            form.instance.precision = CountPrecision.objects.get(contact=contact)
        if contact in ("vm", "te"):
            form.instance.count = 1
        return super(CountDetailUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CountDetailUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-venus-mars"
        context["title"] = _("Modification d'une observation détaillée")
        jsAddAnother = """
        $('#addanother').hide();
        """
        context["js"] = jsAddAnother + js.TimeInput
        return context


class CountDetailDelete(CountDetailEditAuthMixin, DeleteView):
    model = CountDetail
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:sighting_detail", kwargs={"pk": self.object.sighting_id})

    def get_context_data(self, **kwargs):
        context = super(CountDetailDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une observation détaillée")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'observation détaillée"
        )
        return context
