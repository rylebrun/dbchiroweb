import logging

from rest_framework.permissions import BasePermission

logger = logging.getLogger(__name__)

logger.debug(f"type BasePermission {type(BasePermission)}")


class SightingEditPermission(BasePermission):
    """Sighting Edit API permissions

    :param BasePermission: [description]
    :type BasePermission: [type]
    """

    def has_permission(self, request, view) -> bool:
        """[summary]

        :param request: [description]
        :type request: [type]
        :param view: [description]
        :type view: [type]
        :return: [description]
        :rtype: [type]
        """
        logger.debug(f"type self {type(self)}")
        logger.debug(f"type request {type(request)}")
        logger.debug(f"type view {type(view)}")
        if view.action in ["list", "create", "retrieve", "update", "partial_update", "destroy"]:
            return request.user.is_authenticated
        else:
            return False

    def has_object_permission(self, request, view, obj) -> bool:
        user = request.user
        logger.debug(f"Action: {view.action}")
        logger.debug(f"type self {type(self)}")
        logger.debug(f"type request {type(request)}")
        logger.debug(f"type view {type(view)}")
        logger.debug(f"type obj {type(obj)}")
        # Deny actions on objects if the user is not authenticated
        if not user.is_authenticated:
            return False
        if view.action in ["retrieve", "update", "partial_update", "destroy"]:
            logger.debug(f"cond: user == obj.created_by > {user == obj.created_by}")
            logger.debug(
                f"cond: user == obj.session.created_by > {user == obj.session.created_by}"
            )
            logger.debug(f"cond: user.edit_all_data > {user.edit_all_data}")
            logger.debug(f"cond: user.is_staff > {user.is_staff}")
            logger.debug(
                f"cond: (user.is_resp and obj.session.place.territory in user.resp_territory.all()) > {(user.is_resp and obj.session.place.territory in user.resp_territory.all())}"
            )
            return (
                user == obj.created_by
                or user == obj.session.created_by
                or user.is_staff
                or user.edit_all_data
                or (user.is_resp and obj.session.place.territory in user.resp_territory.all())
            )
        else:
            return False
