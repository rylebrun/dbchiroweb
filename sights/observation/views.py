"""
    Vues de l'application Sights
"""

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError
from django.db.models import Prefetch, Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django_tables2 import SingleTableView
from django_tables2.export import ExportMixin

from geodata.models import Territory
from sights.forms import SightingForm
from sights.mixins import (
    AdvancedUserViewMixin,
    SessionEditAuthMixin,
    SightingEditAuthMixin,
    SightingViewAuthMixin,
)
from sights.models import CountDetail, Device, Session, Sighting
from sights.tables import (
    CountDetailAcousticTable,
    CountDetailBiomTable,
    CountDetailOtherTable,
    CountDetailTelemetryTable,
    SessionDeviceTable,
    SightingTable,
)

IMAGE_FILE_TYPES = ["png", "jpg", "jpeg"]
DOCUMENT_FILE_TYPES = ["doc", "docx", "odt", "pdf"]


class SightingCreate(LoginRequiredMixin, CreateView):
    model = Sighting
    form_class = SightingForm
    template_name = "normal_form.html"

    def get_initial(self):
        initial = super(SightingCreate, self).get_initial()
        initial = initial.copy()
        initial["observer"] = self.request.user
        return initial

    def get_form_kwargs(self):
        kwargs = super(SightingCreate, self).get_form_kwargs()
        session_id = self.kwargs.get("pk")
        kwargs["session_id"] = session_id
        kwargs["contact"] = Session.objects.get(id_session=session_id).contact.code
        kwargs["main_observer"] = Session.objects.get(id_session=session_id).main_observer
        kwargs["other_observer"] = Session.objects.get(id_session=session_id).other_observer
        return kwargs

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        session_id = self.kwargs.get("pk")
        form.instance.session_id = session_id
        form.instance.observer_id = Session.objects.get(id_session=session_id).main_observer.id
        contact = Session.objects.get(id_session=session_id).contact.code
        if contact == "du":
            #  Si le type de contact est détection acoustique, alors le 'total_count' vaut 1
            form.instance.total_count = 1
        try:
            return super(SightingCreate, self).form_valid(form)
        except IntegrityError as e:
            messages.error(self.request, e.__cause__)
            return HttpResponseRedirect(self.request.path)
        # except IntegrityError as e:
        #     # messages.error(self.request, e.__cause__)
        #     messages.error(self.request, _('Cette observation existe déjà'))
        #     return HttpResponseRedirect(self.request.path)
        return super(SightingCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SightingCreate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = "Ajout d'une observation"
        context[
            "js"
        ] = """
        """
        return context

    # Improve workflow - Go to count detail
    def get_success_url(self):
        session_id = self.object.session_id
        id_sighting = self.object.id_sighting
        if self.request.method == "POST" and "_addanother" in self.request.POST:
            return reverse_lazy("sights:sighting_create", kwargs={"pk": session_id})
        elif self.request.method == "POST" and "gotoCount" in self.request.POST:
            return reverse_lazy("sights:countdetail_create", kwargs={"pk": id_sighting})
        return reverse_lazy("sights:session_detail", kwargs={"pk": session_id})


class SightingUpdate(SightingEditAuthMixin, UpdateView):
    model = Sighting
    form_class = SightingForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        session_id = self.object.session_id
        form.instance.observer_id = Session.objects.get(id_session=session_id).main_observer.id
        contact = Session.objects.get(id_session=session_id).contact.code
        if contact == "du":
            #  Si le type de contact est détection acoustique, alors le 'total_count' vaut 1
            form.instance.total_count = 1
        try:
            return super(SightingUpdate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette observation existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(SightingUpdate, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(SightingUpdate, self).get_form_kwargs()
        session_id = self.object.session_id
        kwargs["contact"] = Session.objects.get(id_session=session_id).contact.code
        kwargs["session_id"] = session_id
        kwargs["main_observer"] = Session.objects.get(id_session=session_id).main_observer
        kwargs["other_observer"] = Session.objects.get(id_session=session_id).other_observer
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(SightingUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = "Modification d'une observation"
        context[
            "js"
        ] = """
        """
        return context


class SightingDelete(SightingEditAuthMixin, DeleteView):
    model = Sighting
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:session_detail", kwargs={"pk": self.object.session_id})

    def get_context_data(self, **kwargs):
        context = super(SightingDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une observation")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer l'observation")
        return context


class SightingDetail(SightingViewAuthMixin, DetailView):
    model = Sighting
    template_name = "sighting_detail.html"

    def get_context_data(self, **kwargs):
        context = super(SightingDetail, self).get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        # Rendu des sessions
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = _("Détail d'une observation")
        context[
            "js"
        ] = """
        """
        # Rendu des observations
        context["countdetailicon"] = "fa fa-eye fa-fw"
        context["countdetailtitle"] = _("Détails de l'observation")
        context["countdetailcount"] = CountDetail.objects.filter(sighting=pk).distinct().count()
        if Sighting.objects.get(id_sighting=pk).session.contact.code:
            contact = Sighting.objects.get(id_sighting=pk).session.contact.code
            context["contact"] = contact
        else:
            contact = "nc"
        if contact in ("vm", "ca"):
            countdetailtable = CountDetailBiomTable(CountDetail.objects.filter(sighting=pk))
        elif contact == "du":
            countdetailtable = CountDetailAcousticTable(CountDetail.objects.filter(sighting=pk))
        elif contact == "te":
            countdetailtable = CountDetailTelemetryTable(CountDetail.objects.filter(sighting=pk))
        else:
            countdetailtable = CountDetailOtherTable(CountDetail.objects.filter(sighting=pk))
        context["countdetailtable"] = countdetailtable
        # Rendu des dispositifs
        context["deviceicon"] = "fi-target"
        context["devicetitle"] = _("Sessions d'inventaires")
        context["devicecount"] = Device.objects.filter(session=pk).count()
        context["devicetable"] = SessionDeviceTable(Device.objects.filter(session=pk))
        return context


class SightingMyList(LoginRequiredMixin, ExportMixin, SingleTableView):
    table_class = SightingTable
    template_name = "table.html"
    table_pagination = {"per_page": 25}
    export_name = _("mes_observations")

    def get_context_data(self, **kwargs):
        context = super(SightingMyList, self).get_context_data(**kwargs)
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = _("Mes observations")
        context[
            "js"
        ] = """
        """
        context["counttitle"] = _("Nombre d'observations")
        context["export"] = True
        context["count"] = (
            Sighting.objects.filter(
                Q(created_by=loggeduser)
                | Q(observer=loggeduser)
                | Q(session__other_observer__username__contains=loggeduser.username)
            )
            .distinct()
            .count()
        )
        return context

    def get_queryset(self):
        loggeduser = self.request.user
        return (
            Sighting.objects.filter(
                Q(created_by=loggeduser)
                | Q(observer=loggeduser)
                | Q(session__other_observer__username__contains=loggeduser.username)
            )
            .distinct()
            .order_by("-timestamp_update")
            .select_related("created_by")
            .select_related("updated_by")
            .select_related("observer")
            .select_related("session")
            .select_related("codesp")
            .prefetch_related("session__place")
            .prefetch_related("session__place__municipality")
            .prefetch_related("session__place__territory")
            .prefetch_related("session__place__type")
            .prefetch_related("session__contact")
            .prefetch_related("session__main_observer")
            .prefetch_related("session__other_observer")
            .prefetch_related("session__study")
        )


class SightingUserList(AdvancedUserViewMixin, ExportMixin, SingleTableView):
    table_class = SightingTable
    template_name = "table.html"
    table_pagination = {"per_page": 25}
    export_name = "export_user_sighting.ext"

    def get_context_data(self, **kwargs):
        context = super(SightingUserList, self).get_context_data(**kwargs)
        user = get_user_model().objects.get(id=self.kwargs.get("pk"))
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = _("Observations de ") + user.get_full_name()
        context[
            "js"
        ] = """
        """
        context["counttitle"] = _("Nombre d'observations")
        context["export"] = True
        userlist = Sighting.objects.filter(
            Q(created_by=user)
            | Q(observer=user)
            | Q(session__other_observer__username__contains=user.username)
        )
        if loggeduser.access_all_data or loggeduser.edit_all_data:
            context["count"] = userlist.distinct().count()
        elif loggeduser.is_resp:
            context["count"] = (
                userlist.filter(session__place__territory__in=loggeduser.resp_territory.all())
                .distinct()
                .count()
            )
        return context

    def get_queryset(self):
        user = get_user_model().objects.get(id=self.kwargs.get("pk"))
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        userlist = (
            Sighting.objects.filter(
                Q(created_by=user)
                | Q(observer=user)
                | Q(session__other_observer__username__contains=user.username)
            )
            .select_related("created_by")
            .select_related("updated_by")
            .select_related("observer")
            .select_related("session")
            .select_related("codesp")
            .prefetch_related("session__place")
            .prefetch_related("session__place__municipality")
            .prefetch_related("session__place__territory")
            .prefetch_related("session__place__type")
            .prefetch_related("session__contact")
            .prefetch_related("session__main_observer")
            .prefetch_related("session__other_observer")
            .prefetch_related("session__study")
        )
        if loggeduser.access_all_data or loggeduser.edit_all_data:
            return userlist.distinct().order_by("-timestamp_update")
        elif loggeduser.is_resp:
            return (
                userlist.filter(session__place__territory__in=loggeduser.resp_territory.all())
                .distinct()
                .order_by("-timestamp_update")
            )


class SightingSearch(LoginRequiredMixin, TemplateView):
    template_name = "sighting_search.html"

    def get_context_data(self, **kwargs):
        context = super(SightingSearch, self).get_context_data(**kwargs)
        enable_territory = Territory.objects.filter(coverage=True).all()
        print(enable_territory)
        context["enable_territory"] = enable_territory
        context["icon"] = "fa fa-fw fa-eye"
        context["title"] = _("Rechercher une observation")
        context[
            "js"
        ] = """
        """
        return context


class SightingEditForm(SessionEditAuthMixin, DetailView):
    model = Session
    template_name = "sighting_form.html"

    def get_context_data(self, **kwargs):
        context = super(SightingEditForm, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = "Edition des observations"
        return context
