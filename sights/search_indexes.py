import datetime

from haystack import indexes

from sights.models import Place


class PlaceIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    place = indexes.CharField(model_attr="name")
    municipality = indexes.CharField()

    def get_model(self):
        return Place

    def prepare_municipality(self, obj):
        return obj.municipality.name

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(timestamp_create__lte=datetime.datetime.now())
