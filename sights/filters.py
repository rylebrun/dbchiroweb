import django_filters
from dal import autocomplete
from django.utils.translation import ugettext_lazy as _

from dicts.models import MetaPlaceType, TypePlace
from sights.forms import MetaPlaceSearchFilterForm, PlaceSearchFilterForm
from sights.models import Place


class PlaceFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(label=_("Nom de la localité"), lookup_expr="icontains")
    municipality = django_filters.CharFilter(
        label=_("Zonage"),
        widget=autocomplete.Select2Multiple(
            url="api:area_autocomplete", attrs={"class": "listselect2"}
        ),
    )
    territory = django_filters.CharFilter(
        label=_("Département"),
        widget=autocomplete.ListSelect2(
            url="api:territory_autocomplete", attrs={"class": "listselect2"}
        ),
    )
    type = django_filters.ModelChoiceFilter(
        label=_("Type de localité"), queryset=TypePlace.objects.all()
    )
    metaplace = django_filters.CharFilter(
        label=_("Métasite"),
        widget=autocomplete.ListSelect2(
            url="api:metaplace_autocomplete", attrs={"class": "listselect2"}
        ),
    )
    metaplace_type = django_filters.ModelChoiceFilter(
        label=_("Type de métasite"), queryset=MetaPlaceType.objects.all()
    )

    class Meta:
        model = Place
        fields = ["name", "municipality", "territory", "type", "metaplace"]
        order_by = ["name"]
        form = PlaceSearchFilterForm


class MetaPlaceFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(label=_("Nom du métasite"), lookup_expr="icontains")
    type = django_filters.ModelChoiceFilter(
        label=_("Type de métasite"), queryset=MetaPlaceType.objects.all()
    )

    class Meta:
        model = Place
        fields = ["name", "type"]
        order_by = ["name"]
        form = MetaPlaceSearchFilterForm
