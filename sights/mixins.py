"""
    Mixins du projet
"""

import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch, Q
from django.shortcuts import redirect
from django.urls import reverse
from rest_framework_gis.pagination import GeoJsonPagination

from dbchiro.settings.base import SEE_ALL_NON_SENSITIVE_DATA
from geodata.models import Municipality
from sights.models import Place, Sighting

# Get an instance of a logger
logger = logging.getLogger(__name__)


class ModifyDataAuthMixin(LoginRequiredMixin):
    """
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        if (
            get_user_model().objects.get(id=request.user.id).filter(edit_all_data=True)
            or self.obj.created_by == request.user
        ):
            logger.debug("access granted")
            return super(ModifyDataAuthMixin, self).dispatch(request, *args, **kwargs)
        else:
            return redirect("core:update_unauth")


class AdvancedUserViewMixin(LoginRequiredMixin):
    """
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        user = get_user_model().objects.get(id=request.user.id)
        if user.edit_all_data or user.access_all_data or user.is_resp or user.is_superuser:
            logger.debug("access granted")
            return super(AdvancedUserViewMixin, self).dispatch(request, *args, **kwargs)
        else:
            return redirect("core:update_unauth")


class PlaceViewAuthMixin(LoginRequiredMixin):
    """
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        logger.debug(f"request.user {request.user} {dir(request.user)}")
        logger.debug(f"self {self} {dir(self)}")
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        else:
            if (
                user.edit_all_data
                or self.obj.created_by == user
                or (self.obj.is_hidden and user in self.obj.authorized_user.all())
                or (user.is_resp and self.obj.territory in user.resp_territory.all())
                or not self.obj.is_hidden
            ):
                logger.debug("access granted")
                return super(PlaceViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class PlaceEditAuthMixin:
    """
    Permission d'éditer la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or (loggeduser.is_resp and self.obj.territory in loggeduser.resp_territory.all())
                or self.obj.created_by == loggeduser
            ):
                return super(PlaceEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:update_unauth")


class PlaceDetailViewAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or loggeduser.access_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
            ):
                return super(PlaceDetailViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class PlaceDetailEditAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
            ):
                return super(PlaceDetailEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:update_unauth")


class TreeGiteViewAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or loggeduser.access_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.tree.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
            ):
                return super(TreeGiteViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class TreeGiteEditAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.tree.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
            ):
                return super(TreeGiteEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:update_unauth")


class SessionViewAuthMixin:
    """
    Permet de voir la session dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or loggeduser.access_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
                or self.obj.main_observer == loggeduser
                or loggeduser in self.obj.other_observer.all()
                or (
                    SEE_ALL_NON_SENSITIVE_DATA
                    and not self.obj.is_confidential
                    and not self.obj.place.is_hidden
                )
            ):
                return super(SessionViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class SessionEditAuthMixin:
    """
    Permet de voir la session dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        ou L'utilisateur peut voir toutes les données,
        ou L'utilisateur est responsable du territoire de la localité de cette session,
        ou L'utilisateur est créateur de la session
        ou L'utilisateur est l'observateur principal de la session
        ou L'utilisateur est l'un des autres observateurs de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
                or self.obj.main_observer == loggeduser
                or loggeduser in self.obj.other_observer.all()
            ):
                return super(SessionEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:update_unauth")


class SightingViewAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or loggeduser.access_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.session.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
                or self.obj.session.main_observer == loggeduser
                or loggeduser in self.obj.session.other_observer.all()
                or (
                    SEE_ALL_NON_SENSITIVE_DATA
                    and not self.obj.session.is_confidential
                    and (
                        not self.obj.session.place.is_hidden
                        or loggeduser in self.obj.session.place.authorized_user.all()
                    )
                )
            ):
                return super(SightingViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class SightingEditAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur  :get_user_model():is_superuser is True,
        L'utilisateur peut voir toutes les données :get_user_model():edit_all_data is True,
        L'utilisateur est responsable du territoire de la localité de cette session :get_user_model():is_resp is True & territory in :get_user_model():resp_territory,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'observateur principal de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.session.place.territory in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
                or self.obj.session.main_observer == loggeduser
            ):
                return super(SightingEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:update_unauth")


class CountDetailViewAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'un des observateurs de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or loggeduser.access_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.sighting.session.place.territory
                    in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
                or self.obj.sighting.session.main_observer == loggeduser
                or loggeduser in self.obj.sighting.session.other_observer.all()
                or (
                    SEE_ALL_NON_SENSITIVE_DATA
                    and not self.obj.sighting.session.is_confidential
                    and not self.obj.sighting.session.place.is_hidden
                )
            ):
                return super(CountDetailViewAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class CountDetailEditAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur  :get_user_model():is_superuser is True,
        L'utilisateur peut voir toutes les données :get_user_model():edit_all_data is True,
        L'utilisateur est responsable du territoire de la localité de cette session :get_user_model():is_resp is True & territory in :get_user_model():resp_territory,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'observateur principal de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.is_superuser
                or loggeduser.edit_all_data
                or (
                    loggeduser.is_resp
                    and self.obj.sighting.session.place.territory
                    in loggeduser.resp_territory.all()
                )
                or self.obj.created_by == loggeduser
                or self.obj.sighting.session.main_observer == loggeduser
            ):
                return super(CountDetailEditAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:update_unauth")


class LargeGeoJsonPageNumberPagination(GeoJsonPagination):
    page_size = 100
    page_size_query_param = "page_size"
    max_page_size = 5000


class PlaceFilteringMixin:
    """Mixin used for Place lists filtering"""

    def get_queryset(self, *args, **kwargs):
        qs = super(PlaceFilteringMixin, self).get_queryset()

        municipality = self.request.query_params.get("municipality", None)
        name = self.request.query_params.get("name", None)
        type = self.request.query_params.get("type", None)
        alt_min = self.request.query_params.get("alt_min", None)
        alt_max = self.request.query_params.get("alt_max", None)
        gite = self.request.query_params.get("gite", None)
        creator = self.request.query_params.get("creator", None)
        created_by_id = self.request.query_params.get("created_by_id", None)
        only_mine = self.request.query_params.get("only_mine", None)
        metaplace = self.request.query_params.get("metaplace", None)
        convention = self.request.query_params.get("convention", None)
        managed = self.request.query_params.get("managed", None)
        territory = self.request.query_params.get("territory", None)
        logger.debug(f"QUERY_PARAMS {self.request.query_params}")
        qs = (
            qs.filter(
                Q(municipality__name__icontains=municipality)
                | Q(municipality__code__icontains=municipality)
            )
            if municipality is not None
            else qs
        )
        qs = qs.filter(name__icontains=name) if name is not None else qs
        qs = (
            qs.filter(
                Q(type__code__icontains=type)
                | Q(type__category__icontains=type)
                | Q(type__descr__icontains=type)
            )
            if type is not None
            else qs
        )

        qs = qs.filter(altitude__gte=alt_min) if alt_min is not None else qs
        qs = qs.filter(altitude__lte=alt_max) if alt_max is not None else qs
        qs = qs.filter(created_by__username__icontains=creator) if creator is not None else qs
        if created_by_id is not None:
            qs = qs.filter(created_by=created_by_id)
        if only_mine is not None:
            qs = qs.filter(created_by=self.request.user.id)
            logger.debug(f"ONLY_MINE{qs}")
        qs = qs.filter(is_gite=True) if gite is not None else qs
        qs = (
            qs.filter(
                Q(metaplace__name__icontains=metaplace)
                | Q(metaplace__type__code__icontains=metaplace)
                | Q(metaplace__type__category__icontains=metaplace)
                | Q(metaplace__type__descr__icontains=metaplace)
            )
            if metaplace is not None
            else qs
        )

        qs = qs.filter(convention=True) if convention is not None else qs
        qs = qs.filter(is_managed=True) if managed is not None else qs
        qs = (
            qs.filter(
                Q(created_by__first_name__icontains=creator)
                | Q(created_by__last_name__icontains=creator)
                | Q(created_by__username__icontains=creator)
            )
            if creator is not None
            else qs
        )
        qs = qs.filter(territory__id=territory) if territory is not None else qs

        logger.debug(f"Q {qs}")
        return qs


class PlaceListPermissionsMixin:
    def get_queryset(self, *args, **kwargs):
        qs = super(PlaceListPermissionsMixin, self).get_queryset()

        user = self.request.user
        if user.access_all_data or user.edit_all_data or user.is_superuser:
            qs = qs

        # Right access
        else:
            qs = (
                qs.filter(
                    Q(created_by=user)
                    | Q(territory__in=user.resp_territory.all())
                    | Q(
                        ~Q(territory__in=user.resp_territory.all())
                        & (Q(is_hidden=False) | Q(authorized_user=user))
                    )
                )
                .distinct()
                .order_by("-timestamp_update")
            )

        return qs


class PlaceFilteredListWithPermissions(
    PlaceListPermissionsMixin,
    PlaceFilteringMixin,
):
    pass


class SightingFilteringMixin:
    """Mixin used for Sighting lists filtering"""

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        # qs = Sighting.objects

        municipality = self.request.query_params.get("municipality", None)
        name = self.request.query_params.get("name", None)
        specie = self.request.query_params.get("specie", None)
        count_min = self.request.query_params.get("count_min", None)
        count_max = self.request.query_params.get("count_max", None)
        place_type = self.request.query_params.get("place_type", None)
        contact_type = self.request.query_params.get("contact_type", None)
        alt_min = self.request.query_params.get("alt_min", None)
        alt_max = self.request.query_params.get("alt_max", None)
        gite = self.request.query_params.get("gite", None)
        creator = self.request.query_params.get("creator", None)
        metaplace = self.request.query_params.get("metaplace", None)
        date_min = self.request.query_params.get("date_min", None)
        date_max = self.request.query_params.get("date_max", None)
        period = self.request.query_params.get("period", None)
        sex = self.request.query_params.get("sex", None)
        sexual_status = self.request.query_params.get("sexual_state", None)
        age = self.request.query_params.get("age", None)
        breed_colo = self.request.query_params.get("breed_colo", None)
        territory = self.request.query_params.get("territory", None)

        qs = (
            qs.filter(
                Q(session__place__municipality__name__icontains=municipality)
                | Q(session__place__municipality__code__icontains=municipality)
            )
            if municipality is not None
            else qs
        )
        qs = qs.filter(session__place__name__icontains=name) if name is not None else qs
        qs = (
            qs.filter(
                Q(session__place__type__code__icontains=place_type)
                | Q(session__place__type__category__icontains=place_type)
                | Q(session__place__type__descr__icontains=place_type)
            )
            if place_type is not None
            else qs
        )
        qs = (
            qs.filter(
                Q(session__contact__code__icontains=contact_type)
                | Q(session__contact__descr__icontains=contact_type)
            )
            if contact_type is not None
            else qs
        )
        qs = qs.filter(session__place__altitude__gte=alt_min) if alt_min is not None else qs
        qs = qs.filter(session__place__altitude__lte=alt_max) if alt_max is not None else qs
        qs = qs.filter(created_by__username__icontains=creator) if creator is not None else qs
        qs = qs.filter(session__place__is_gite=True) if gite is not None else qs
        qs = (
            qs.filter(
                Q(session__place__metaplace__name__icontains=metaplace)
                | Q(session__place__metaplace__type__code__icontains=metaplace)
                | Q(session__place__metaplace__type__category__icontains=metaplace)
                | Q(session__place__metaplace__type__descr__icontains=metaplace)
            )
            if metaplace is not None
            else qs
        )
        qs = (
            qs.filter(
                Q(codesp__sci_name__icontains=specie)
                | Q(codesp__codesp__icontains=specie)
                | Q(codesp__common_name_fr__icontains=specie)
            )
            if specie is not None
            else qs
        )
        qs = qs.filter(total_count__gte=count_min) if count_min is not None else qs
        qs = qs.filter(total_count__lte=count_max) if count_max is not None else qs
        qs = (
            qs.filter(
                Q(countdetails__sex__code__icontains=sex)
                | Q(countdetails__sex__descr__icontains=sex)
            )
            if sex is not None
            else qs
        )
        qs = (
            qs.filter(
                Q(countdetails__age__code__icontains=age)
                | Q(countdetails__age__descr__icontains=age)
            )
            if age is not None
            else qs
        )
        qs = (
            qs.filter(
                Q(countdetails__mammelle__code__icontains=sexual_status)
                | Q(countdetails__mammelle__descr__icontains=sexual_status)
                | Q(countdetails__gestation__code__icontains=sexual_status)
                | Q(countdetails__gestation__descr__icontains=sexual_status)
                | Q(countdetails__testicule__code__icontains=sexual_status)
                | Q(countdetails__testicule__descr__icontains=sexual_status)
                | Q(countdetails__etat_sexuel__icontains=sexual_status)
                | Q(countdetails__etat_sexuel__icontains=sexual_status)
            )
            if sexual_status is not None
            else qs
        )
        qs = (
            qs.filter(Q(session__date_start__gte=date_min) | Q(session__date_end__gte=date_min))
            if date_min is not None
            else qs
        )
        qs = (
            qs.filter(Q(session__date_start__lte=date_max) | Q(session__date_end__lte=date_max))
            if date_max is not None
            else qs
        )
        qs = qs.filter(period__icontains=period) if period is not None else qs
        qs = qs.filter(breed_colo=True) if breed_colo is not None else qs
        qs = qs.filter(period__icontains=period) if period is not None else qs
        qs = qs.filter(breed_colo=True) if breed_colo is not None else qs
        qs = qs.filter(session__place__territory__id=territory) if territory is not None else qs
        logger.debug(f"Q {qs}")
        return qs


class SightingListPermissionsMixin(object):
    """Mixin used for Sighting lists permissions"""

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()

        logged_user = self.request.user
        user = get_user_model().objects.get(id=logged_user.id)

        if user.access_all_data or user.edit_all_data or user.is_superuser:
            qs = qs.distinct().order_by("-timestamp_update")
        else:
            if SEE_ALL_NON_SENSITIVE_DATA:
                if user.is_resp:
                    qs = (
                        qs.filter(
                            Q(
                                Q(created_by=user)
                                | Q(observer=user)
                                | Q(session__main_observer=user)
                                | Q(session__other_observer=user)
                            )
                            | Q(session__place__territory__in=user.resp_territory.all())
                            | Q(
                                ~Q(session__place__territory__in=user.resp_territory.all())
                                & (
                                    (Q(session__is_confidential=False))
                                    & (
                                        Q(session__place__is_hidden=False)
                                        | Q(session__place__authorized_user=user)
                                    )
                                )
                            )
                        )
                        .distinct()
                        .order_by("-timestamp_update")
                    )
                else:
                    qs = (
                        qs.filter(
                            (
                                Q(created_by=user)
                                | Q(observer=user)
                                | Q(session__main_observer=user)
                                | Q(session__other_observer=user)
                            )
                            | (
                                (Q(session__is_confidential=False))
                                & (
                                    Q(session__place__is_hidden=False)
                                    | Q(session__place__authorized_user=user)
                                )
                            )
                        )
                        .distinct()
                        .order_by("-timestamp_update")
                    )
            else:
                if user.is_resp:
                    qs = (
                        qs.filter(
                            Q(
                                Q(created_by=user)
                                | Q(observer=user)
                                | Q(session__main_observer=user)
                                | Q(session__other_observer=user)
                            )
                            | Q(session__place__territory__in=user.resp_territory.all())
                        )
                        .distinct()
                        .order_by("-timestamp_update")
                    )
                else:
                    qs = (
                        qs.filter(
                            Q(created_by=user)
                            | Q(observer=user)
                            | Q(session__other_observer__username__contains=user.username)
                        )
                        .distinct()
                        .order_by("-timestamp_update")
                    )

        return qs


class SightingFilteredListWithPermissions(
    SightingFilteringMixin,
    SightingListPermissionsMixin,
):
    pass
