from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_gis import serializers

from accounts.models import Profile
from dicts.models import MetaPlaceType, TypePlace
from geodata.models import Municipality
from sights.models import MetaPlace, Place


class CountModelMixin(object):
    """
    Count a queryset.
    """

    @action(detail=False)
    def count(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        content = {"count": queryset.count()}
        return Response(content)


class PlaceMunicipalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Municipality
        fields = ("name", "code")


class PlaceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypePlace
        fields = ("code", "category", "descr")


class PlaceUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ("get_full_name",)


class PlaceMetaplaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetaPlace
        fields = ("id_metaplace", "name")


class MetaplaceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetaPlaceType
        fields = ("id", "category", "descr")


class MetaplaceUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ("get_full_name",)


class PlaceSession:
    pass


class PlaceSerializer(serializers.GeoFeatureModelSerializer):
    municipality_data = PlaceMunicipalitySerializer(source="municipality")
    type_data = PlaceTypeSerializer(source="type")
    creator = PlaceUserSerializer(source="created_by")
    metaplace_data = PlaceMetaplaceSerializer(source="metaplace")

    class Meta:
        model = Place
        depth = 1
        geo_field = "geom"
        fields = (
            "id_place",
            "name",
            "altitude",
            "municipality_data",
            "metaplace_data",
            "type_data",
            "is_managed",
            "convention",
            "geom",
            "creator",
            "timestamp_create",
        )


class MetaPlaceSerializer(serializers.GeoFeatureModelSerializer):
    type_data = MetaplaceTypeSerializer(source="type")
    creator = MetaplaceUserSerializer(source="created_by")

    class Meta:
        model = MetaPlace
        depth = 1
        geo_field = "geom"
        fields = (
            "id_metaplace",
            "name",
            "type_data",
            "creator",
        )
