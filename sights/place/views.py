"""
    Vues de l'application Sights
"""

import datetime
import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.gis.geos import Point
from django.core.files.storage import FileSystemStorage
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from core import js
from dbchiro.settings.base import SEE_ALL_NON_SENSITIVE_DATA
from geodata.models import Territory

from ..forms import (
    BridgeForm,
    BuildForm,
    CaveForm,
    PlaceForm,
    PlaceManagementForm,
    TreeForm,
    TreeGiteForm,
)
from ..mixins import (
    PlaceDetailEditAuthMixin,
    PlaceDetailViewAuthMixin,
    PlaceEditAuthMixin,
    PlaceViewAuthMixin,
    TreeGiteEditAuthMixin,
)
from ..models import (
    Bridge,
    Build,
    Cave,
    MetaPlace,
    Place,
    PlaceManagement,
    Session,
    Tree,
    TreeGite,
)
from ..tables import (
    BridgeTable,
    BuildTable,
    CaveTable,
    PlaceManagementTable,
    PlaceSessionTable,
    TreeGiteTable,
    TreeTable,
)

logger = logging.getLogger(__name__)

place_detail_params = {
    "tree": {
        "model": Tree,
        "table": TreeTable,
        "title": "Etats des lieux de l'arbre",
        "icon": "fa fa-tree",
    },
    "building": {
        "model": Build,
        "table": BuildTable,
        "title": _("Etats des lieux du bâtiment"),
        "icon": "mki mki-ruins",
    },
    "bridge": {
        "model": Bridge,
        "table": BridgeTable,
        "title": _("Etats des lieux du pont"),
        "icon": "mki mki-ruins",
    },
    "cave": {
        "model": Cave,
        "table": CaveTable,
        "title": _("Etats des lieux de la cave"),
        "icon": "mki mki-cave_entrance",
    },
}


class PlaceCreate(LoginRequiredMixin, CreateView):
    """Create view for the Place model."""

    model = Place
    form_class = PlaceForm
    file_storage = FileSystemStorage()
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        if self.kwargs.get("pk"):
            metaplace = MetaPlace.objects.get(pk=self.kwargs.get("pk"))
            form.instance.metaplace = metaplace
        # Check for manual x/y before the leaflet geom
        if form.instance.x and form.instance.y:
            form.instance.geom = Point(form.instance.x, form.instance.y)
        else:
            form.instance.x = form.instance.geom.x
            form.instance.y = form.instance.geom.y
        if form.instance.type:
            if not form.instance.is_gite:
                form.instance.is_gite = True
        return super(PlaceCreate, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(PlaceCreate, self).get_form_kwargs()
        if self.kwargs.get("pk"):
            logger.debug("METAPLACE", self.kwargs.get("pk"))
            kwargs["metaplace"] = self.kwargs.get("pk")
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(PlaceCreate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Ajout d'une localité")
        context[
            "js"
        ] = """

        """
        return context

        # Improve workflow - Go to Session
        def get_success_url(self):
            id_place = self.object.id_place
            if self.request.method == "POST" and "gotoSession" in self.request.POST:
                return reverse_lazy("sights:session_create", kwargs={"pk": id_place})
            return reverse_lazy("sights:place_detail", kwargs={"pk": id_place})


class PlaceDetail(PlaceViewAuthMixin, DetailView):
    model = Place
    template_name = "place_detail.html"

    def get_context_data(self, **kwargs):
        context = super(PlaceDetail, self).get_context_data(**kwargs)
        loggeduser = self.request.user
        logger.debug(f"loggeduser {loggeduser} f{dir(loggeduser)}")
        pk = self.kwargs.get("pk")
        placeobject = (
            Place.objects.select_related("territory")
            .prefetch_related("authorized_user")
            .select_related("created_by")
        ).get(id_place=pk)
        sessions = (
            Session.objects.filter(place=pk)
            .select_related("main_observer")
            .select_related("contact")
            .select_related("created_by")
            .prefetch_related("other_observer")
            .prefetch_related("observations")
            .prefetch_related("observations__codesp")
        )
        # placeneighbour = Place.objects.filter(geom__within=(placeobject.geom, D(m=50)))
        placedetail = False
        #  Etats des lieux à afficher en fonction du type de localité (tree, build, bridge, cave)
        if placeobject.type:
            if placeobject.type.category in ("tree", "building", "bridge", "cave"):
                placedetail = True
                category_param = place_detail_params[placeobject.type.category]
                placedetailcount = category_param["model"].objects.filter(place=pk).count()
                placedetailtable = category_param["table"](
                    category_param["model"].objects.filter(place=pk)
                )
                placedetailicon = category_param["icon"]
                placedetailtitle = category_param["title"]
                placedetailcreateurl = (
                    f"{{% url 'sights:{placeobject.type.category}_create' pk=object.pk %}}"
                )
        # Données de session disponibles pour l'observateur
        if loggeduser.access_all_data or (
            loggeduser.is_resp and placeobject.territory in loggeduser.resp_territory.all()
        ):
            # Si l'utilisateur a un accès total à toute la base
            sessions = sessions
        elif placeobject in Place.objects.filter(
            (Q(is_hidden=True) & Q(authorized_user__username__contains=loggeduser.username))
            | (Q(is_hidden=True) & Q(created_by=loggeduser))
        ):
            # Si le site est caché, seul son créateur et les personnes autorisées peuvent le voir. la
            # liste des sessions correspond aux sessions ou l'utilisateur est créateur ou observateur
            # TODO Gérer la cohérence entre les restrictions de vue de la localité et celles de la table des sessions
            # Filtrage, l'utilisateur doit être créateur ou l'un des observateurs de la session
            sessions = sessions.filter(
                Q(created_by=loggeduser)
                | Q(main_observer=loggeduser)
                | Q(other_observer=loggeduser)
            )
        else:
            # Si l'utilisateur est un simple observateur et que le site est non sensible, il peut en voir le détail et la
            # liste des sessions correspond aux sessions ou l'utilisateur est créateur ou observateur
            # TODO Gérer la cohérence entre les restrictions de vue de la localité et celles de la table des sessions
            logger.debug(
                "%s | %s : site %s non caché"
                % (datetime.datetime.now().strftime("%Y%m%d %I%M%S"), loggeduser, pk)
            )
            if SEE_ALL_NON_SENSITIVE_DATA:
                sessions = sessions.filter(is_confidential=False)
            else:
                sessions = sessions.filter(
                    Q(created_by=loggeduser)
                    | Q(main_observer=loggeduser)
                    | Q(other_observer=loggeduser)
                )

        sessioncount = sessions.count()
        sessiontable = PlaceSessionTable(sessions.order_by("-date_start"))
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Détail d'une localité")
        context[
            "js"
        ] = """
        """
        context["sessionicon"] = "far fa-calendar-alt"
        context["sessiontitle"] = _("Sessions d'inventaires")
        context["sessioncount"] = sessioncount
        context["sessiontable"] = sessiontable
        context["placedetail"] = placedetail
        if placedetail:
            context["placedetailicon"] = placedetailicon
            context["placedetailtitle"] = placedetailtitle
            context["placedetailcount"] = placedetailcount
            context["placedetailtable"] = placedetailtable
            context["placedetailcreateurl"] = placedetailcreateurl
        if placeobject.is_managed:
            context["placemanagementicon"] = "fi-checkbox"
            context["placemanagementtitle"] = _("Actions de gestion")
            context["placemanagementcount"] = PlaceManagement.objects.filter(place=pk).count()
            context["placemanagementtable"] = PlaceManagementTable(
                PlaceManagement.objects.filter(place=pk).order_by("-date")
            )
            context[
                "placemanagementcreateurl"
            ] = "{% url 'management:management_create' pk=object.pk %}"
        return context


class PlaceUpdate(PlaceEditAuthMixin, UpdateView):
    model = Place
    form_class = PlaceForm
    file_storage = FileSystemStorage()
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        if not form.instance.geom:
            if form.instance.x and form.instance.y:
                form.instance.geom = Point(form.instance.x, form.instance.y)
        else:
            form.instance.x = form.instance.geom.x
            form.instance.y = form.instance.geom.y
        if form.instance.type:
            if not form.instance.is_gite:
                form.instance.is_gite = True
        return super(PlaceUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PlaceUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = "Modification d'une localité"
        context[
            "js"
        ] = """

        """
        return context

    # Improve workflow - Go to Session
    def get_success_url(self):
        id_place = self.object.id_place
        if self.request.method == "POST" and "gotoSession" in self.request.POST:
            return reverse_lazy("sights:session_create", kwargs={"pk": id_place})
        return reverse_lazy("sights:place_detail", kwargs={"pk": id_place})


class PlaceDelete(PlaceEditAuthMixin, DeleteView):
    model = Place
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("blog:home")

    def get_context_data(self, **kwargs):
        context = super(PlaceDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une localité")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer la localité")
        return context


class PlaceList(LoginRequiredMixin, TemplateView):
    template_name = "place_search.html"

    def get_context_data(self, **kwargs):
        context = super(PlaceList, self).get_context_data(**kwargs)
        enable_territory = Territory.objects.filter(coverage=True).all()
        print(enable_territory)
        context["enable_territory"] = enable_territory
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher une localité")
        context["createplacebtn"] = True
        context[
            "js"
        ] = """
        """

        return context


class PlaceMyList(LoginRequiredMixin, TemplateView):
    template_name = "place_search.html"

    def get_context_data(self, **kwargs):
        context = super(PlaceMyList, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher dans mes localités")
        context[
            "js"
        ] = """
        """
        context["only_mine"] = True
        return context


class PlaceManagementCreate(LoginRequiredMixin, CreateView):
    """Create view for the Study model."""

    model = PlaceManagement
    form_class = PlaceManagementForm
    template_name = "normal_form.html"

    def get_initial(self):
        initial = super(PlaceManagementCreate, self).get_initial()
        initial = initial.copy()
        initial["referent"] = self.request.user
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        return super(PlaceManagementCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PlaceManagementCreate, self).get_context_data(**kwargs)
        context["icon"] = "fi-star"
        context["title"] = _("Ajout d'une action de gestion")
        context["js"] = js.DateInput
        return context


class PlaceManagementUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = PlaceManagement
    form_class = PlaceManagementForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(PlaceManagementUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PlaceManagementUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fi-star"
        context["title"] = _("Mise à jour d'une action de gestion")
        context[
            "js"
        ] = """
        $(function () {
            $('.dateinput').fdatepicker({
                format: 'dd/mm/yyyy',
                disableDblClickSelection: true,
                leftArrow: '<i class="fa fa-fw fa-chevron-left"></i>',
                rightArrow: '<i class="fa fa-fw fa-chevron-right"></i>',
            });
        });
        """
        return context


class PlaceManagementDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = PlaceManagement
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("management:study_list")

    def get_context_data(self, **kwargs):
        context = super(PlaceManagementDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une action de gestion")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer l'action")
        return context


#################################################
#               Bridge model views                #
#################################################


class BridgeCreate(LoginRequiredMixin, CreateView):
    model = Bridge
    form_class = BridgeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super(BridgeCreate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(BridgeCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BridgeCreate, self).get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Ajout d'un état des lieux d'un pont")
        context["js"] = js.DateInput
        return context


class BridgeUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Bridge
    form_class = BridgeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(BridgeUpdate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(BridgeUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BridgeUpdate, self).get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Modification d'un état des lieux d'un pont")
        context["js"] = js.DateInput
        return context


class BridgeDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Bridge
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(BridgeDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux d'un pont")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux d'un pont"
        )
        return context


#################################################
#               Tree model views                #
#################################################


class TreeCreate(LoginRequiredMixin, CreateView):
    model = Tree
    form_class = TreeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super(TreeCreate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(TreeCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeCreate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-tree"
        context["title"] = _("Ajout d'un état des lieux d'un arbre")
        context["js"] = js.DateInput
        return context


class TreeUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Tree
    form_class = TreeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(TreeUpdate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(TreeUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeUpdate, self).get_context_data(**kwargs)
        context["icon"] = "mki mki-ruins"
        context["title"] = _("Modification d'un état des lieux d'un arbre")
        context["js"] = js.DateInput
        return context


class TreeDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Tree
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(TreeDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux d'un arbre")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux d'un arbre"
        )
        return context


class TreeDetail(PlaceDetailViewAuthMixin, DetailView):
    model = Tree
    template_name = "tree_detail.html"

    def get_context_data(self, **kwargs):
        context = super(TreeDetail, self).get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        # Rendu des sessions
        context["icon"] = "fi-trees"
        context["title"] = _("Détail de l'état des lieux d'un arbre")
        context[
            "js"
        ] = """
        """
        # Rendu des observations
        context["treegiteicon"] = "fi-home"
        context["treegitetitle"] = _("Détails des gîtes")
        context["treegitecount"] = TreeGite.objects.filter(tree=pk).distinct().count()
        context["treegitetable"] = TreeGiteTable(TreeGite.objects.filter(tree=pk))
        return context


#################################################
#               TreeGite model views                #
#################################################


class TreeGiteCreate(LoginRequiredMixin, CreateView):
    model = TreeGite
    form_class = TreeGiteForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.tree_id = self.kwargs.get("pk")
        try:
            return super(TreeGiteCreate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Ce gîte existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(TreeGiteCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeGiteCreate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-tree"
        context["title"] = _("Ajout d'un détail de gîte arboricole")
        context[
            "js"
        ] = """
        """
        return context


class TreeGiteUpdate(TreeGiteEditAuthMixin, UpdateView):
    model = TreeGite
    form_class = TreeGiteForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        form.instance.tree_id = self.kwargs.get("pk")
        try:
            return super(TreeGiteUpdate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Ce gîte existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(TreeGiteUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeGiteUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-tree"
        context["title"] = _("Modification d'un détail de gîte arboricole")
        context[
            "js"
        ] = """
        """
        return context


class TreeGiteDelete(TreeGiteEditAuthMixin, DeleteView):
    model = Tree
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(TreeGiteDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un détail de gîte arboricole")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer ce détail de gîte")
        return context


#################################################
#               Cave model views                #
#################################################


class CaveCreate(LoginRequiredMixin, CreateView):
    model = Cave
    form_class = CaveForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super(CaveCreate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(CaveCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CaveCreate, self).get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Ajout d'un état des lieux d'une cavité")
        context["js"] = js.DateInput
        return context


class CaveUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Cave
    form_class = CaveForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(CaveUpdate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(CaveUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CaveUpdate, self).get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Modification d'un état des lieux d'une cavité")
        context["js"] = js.DateInput
        return context


class CaveDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Cave
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(CaveDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux d'une cavité")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux d'une cavité"
        )
        return context


#################################################
#               Build model views                #
#################################################


class BuildCreate(LoginRequiredMixin, CreateView):
    model = Build
    form_class = BuildForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super(BuildCreate, self).form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(BuildCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BuildCreate, self).get_context_data(**kwargs)
        context["icon"] = "mki mki-ruins"
        context["title"] = _("Ajout d'un état des lieux de bâtiment")
        context["js"] = js.DateInput
        return context


class BuildUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Build
    form_class = BuildForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(BuildUpdate, self).form_valid(form)
        except IntegrityError:
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super(BuildUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BuildUpdate, self).get_context_data(**kwargs)
        context["icon"] = "mki mki-ruins"
        context["title"] = _("Modification d'un état des lieux de bâtiment")
        context["js"] = js.DateInput
        return context


class BuildDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Build
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(BuildDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux de bâtiment")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux de bâtiment"
        )
        return context
