"""
    Vues de l'application Sights
"""

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django_filters.views import FilterView
from django_tables2 import SingleTableView
from django_tables2.export import ExportMixin

from sights.filters import MetaPlaceFilter
from sights.forms import MetaPlaceForm
from sights.mixins import PlaceEditAuthMixin
from sights.models import MetaPlace, Place
from sights.tables import MetaPlaceTable, PlaceTable

IMAGE_FILE_TYPES = ["png", "jpg", "jpeg"]
DOCUMENT_FILE_TYPES = ["doc", "docx", "odt", "pdf"]


class MetaPlaceCreate(LoginRequiredMixin, CreateView):
    """Créateview for MetaPlace model"""

    model = MetaPlace
    form_class = MetaPlaceForm
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(MetaPlaceCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(MetaPlaceCreate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = "Création d'un métasite"
        context[
            "js"
        ] = """

        """
        return context


class MetaPlaceUpdate(PlaceEditAuthMixin, UpdateView):
    """UpdateView form MetaPlace"""

    model = MetaPlace
    form_class = MetaPlaceForm
    template_name = "leaflet_form.html"

    def get_context_data(self, **kwargs):
        context = super(MetaPlaceUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = "Modification d'un métasite"
        context[
            "js"
        ] = """
        """
        return context


class MetaPlaceDelete(PlaceEditAuthMixin, DeleteView):
    model = MetaPlace
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("blog:home")

    def get_context_data(self, **kwargs):
        context = super(MetaPlaceDelete, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un métasite")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer ce métasite")
        return context


class MetaPlaceList(LoginRequiredMixin, FilterView, ExportMixin, SingleTableView):
    table_class = MetaPlaceTable
    # form_class = MetaPlaceSearchFilterForm
    model = MetaPlace
    template_name = "metaplace_search.html"
    table_pagination = {"per_page": 15}

    filterset_class = MetaPlaceFilter

    def get_context_data(self, **kwargs):
        context = super(MetaPlaceList, self).get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher un méta site")
        context["createmetaplacebtn"] = True
        context[
            "js"
        ] = """
        """
        return context


class MetaPlaceDetail(LoginRequiredMixin, DetailView):
    model = MetaPlace
    template_name = "metaplace_detail.html"

    def get_context_data(self, **kwargs):
        context = super(MetaPlaceDetail, self).get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        placeobjects = Place.objects.filter(metaplace_id=pk)
        placecount = placeobjects.count()
        placetable = PlaceTable(placeobjects)

        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Détail d'un métasite")
        context[
            "js"
        ] = """
        """
        context["placeicon"] = "fas fa-map-marker-alt"
        context["placetitle"] = _("Localités")
        context["placecount"] = placecount
        context["placetable"] = placetable
        return context
