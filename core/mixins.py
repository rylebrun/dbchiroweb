"""
    Mixins
"""

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import redirect
from django.urls import reverse


class ManageAccountAuthMixin:
    """
    Classe mixin de vérification que l'utilisateur possède les droits de créer le compte
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if (
                loggeduser.edit_all_data
                or loggeduser.access_all_data
                or loggeduser.is_resp
                or loggeduser.is_staff
                or loggeduser.is_superuser
            ):
                return super(ManageAccountAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class AdminRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff
